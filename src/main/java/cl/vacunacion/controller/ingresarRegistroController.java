/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.vacunacion.controller;

import cl.vacunacion.dao.VacunasJpaController;
import cl.vacunacion.entity.Vacunas;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author evely
 */
@WebServlet(name = "ingresarRegistroController", urlPatterns = {"/ingresarRegistroController"})
public class ingresarRegistroController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ingresarRegistroController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ingresarRegistroController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("ingresarRegistro.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        

        try {
            String nombre=request.getParameter("name");
            String rut =request.getParameter("rut");
            String apellido=request.getParameter("lastname");
            String edad=request.getParameter("age");
            String tipovacuna=request.getParameter("vaccine");
            System.out.println("rut:"+rut);
            System.out.println("nombre:"+nombre);
            System.out.println("apellido:"+apellido);
            System.out.println("edad:"+edad);
            System.out.println("vacuna:"+tipovacuna);
            
            Vacunas vacuna=new Vacunas();
            vacuna.setRut(rut);
            vacuna.setNombre(nombre);
            vacuna.setApellido(apellido);
            vacuna.setEdad(edad);
            vacuna.setVacuna(tipovacuna);
            
            VacunasJpaController dao = new VacunasJpaController();
            
            dao.create(vacuna);
        } catch (Exception ex) {
            Logger.getLogger(ingresarRegistroController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        request.getRequestDispatcher("index.jsp").forward(request, response); 
            
            

         
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
