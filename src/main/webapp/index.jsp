<%-- 
    Document   : index
    Created on : 05-06-2021, 15:09:56
    Author     : evely
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Vacunación Covid-19</h1>

        <div class="caja-central">
            <a href="verRegistroController">
                <button type="button" class="btns btn btn-primary btn-lg">Ver registro de vacunación</button>
            </a>
            <a href="ingresarRegistroController">
                <button type="button" class="btns btn btn-primary btn-lg">Ingresar registro de vacunación</button>
            </a>  
        </div>

    </body>
</html>
