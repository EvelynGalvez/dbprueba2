<%-- 
    Document   : ingresarRegistro
    Created on : 05-06-2021, 15:48:32
    Author     : evely
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ingresar persona</h1>
        <br>
        <form action="ingresarRegistroController" method="POST">
            <div class="form-group">
                <label for="formGroupExampleInput">Rut</label>
                <input name="rut" type="text" class="form-control" id="formRut" placeholder="Example input">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Nombre</label>
                <input name="name" type="text" class="form-control" id="formName" placeholder="Another input">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Apellido</label>
                <input name="lastname" type="text" class="form-control" id="formLastname" placeholder="Another input">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Edad</label>
                <input name="age" type="text" class="form-control" id="formAge" placeholder="Another input">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Vacuna</label>
                <input name="vaccine" type="text" class="form-control" id="formVaccine" placeholder="Another input">
            </div>
            
             <button type="submit" name="accion" value="ingresar" class="btn btn-success">Ingresar</button>
        </form>
    </body>
</html>
