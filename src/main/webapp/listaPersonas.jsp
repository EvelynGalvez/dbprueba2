<%-- 
    Document   : listaPersonas
    Created on : 05-06-2021, 15:25:55
    Author     : evely
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.vacunacion.entity.Vacunas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Vacunas> ListaVacunas = (List<Vacunas>) request.getAttribute("ListaVacunas");
    Iterator<Vacunas> itVacunas = ListaVacunas.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Registro de vacunación</h1>
        <form  name="form" action="editarRegistroController" method="POST">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Rut</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Vacuna</th>
                        <th scope="col">Seleccionar</th>
                    </tr>
                </thead>
                <tbody>
                    <%while (itVacunas.hasNext()) {
                        Vacunas registro = itVacunas.next();%>
                    <tr>
                        <td><%= registro.getRut()%></td>
                        <td><%= registro.getNombre()%></td>
                        <td><%= registro.getApellido()%></td>
                        <td><%= registro.getEdad()%></td>
                        <td><%= registro.getVacuna()%></td>
                        <td><input type="radio" name="seleccion" value="<%= registro.getRut()%>"> </td>
                    </tr>
                    <%}%>           
                </tbody>
            </table>
            <button name="accion" value="eliminar" type="submit" class="btn btn-danger">Eliminar</button>
        </form>
    </body>
</html>
